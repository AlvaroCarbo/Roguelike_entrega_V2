Roguelike_entrega_V2

New features:
 - Shop room.
 - Random items at the store.
 - Procedurally generated.
 - Melee Weapons implemented.
 - Different levels with their own chambers and opponents.
 - Audio effects and music.
 - Main menu.
 - Persistence of the game.
 - Wave Spawners in the room.
 - Door logic.
 - Cinematographic logic.
 - Sequence of play: initial room - main room - final room.
