﻿using UnityEngine;

[CreateAssetMenu(menuName = "UI/AudioData", fileName = "AudioData", order = 1)]
public class AudioData : ScriptableObject
{
    public bool mute;
}