﻿using UnityEngine;

public class AudioEffectsManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] clips;

    private AudioSource source;

    public static AudioEffectsManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this) Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlaySound(int index)
    {
        source.clip = clips[index];
        source.Play();
    }
}