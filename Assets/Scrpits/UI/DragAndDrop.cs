﻿using UnityEngine;
using UnityEngine.Serialization;

public class DragAndDrop : MonoBehaviour
{
    [FormerlySerializedAs("_inputData")] [SerializeField]
    private InputData inputData;

    private bool correctPositon;
    private bool isMoving;

    private Vector3 lastPosition;

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (!correctPositon) gameObject.transform.position = lastPosition;
        if (isMoving)
            gameObject.transform.position = inputData.mouseScreenPoint;
        else
            lastPosition = gameObject.transform.position;
    }

    public void OnMouseDown()
    {
        OnMoveChange();
    }

    public void OnMouseUp()
    {
        OnMoveChange();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        OnTriggerChange();
        lastPosition = collision.gameObject.transform.position;
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        OnTriggerChange();
    }

    private void OnMoveChange()
    {
        isMoving = !isMoving;
    }

    private void OnTriggerChange()
    {
        correctPositon = !correctPositon;
    }
}