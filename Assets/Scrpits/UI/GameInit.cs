﻿using TMPro;
using UnityEngine;

public class GameInit : MonoBehaviour
{
    [SerializeField] private PlayerData playerData;
    [SerializeField] private TMP_Text highScoreTMPText;
    [SerializeField] private TMP_Text coinsTMPText;
    [SerializeField] private WeaponData[] starterWeapons;

    private void FixedUpdate()
    {
        playerData.coins = PlayerPrefs.GetInt("coins");
        playerData.highScore = PlayerPrefs.GetInt("HighScore");
        highScoreTMPText.text = $"High Score: {playerData.highScore}";
        coinsTMPText.text = $"Coins: {playerData.coins}";
    }

    public void NewGame()
    {
        playerData.coins = 0;
        playerData.highScore = 0;
        playerData.dashCdTime = 2;
        PlayerPrefs.SetInt("coins", 0);
        PlayerPrefs.SetInt("HighScore", 0);
        PlayerPrefs.Save();
        playerData.playerWeaponsData.weaponIndex = 0;
        playerData.playerWeaponsData.playerWeapons.Clear();
        foreach (var starter in starterWeapons)
            playerData.playerWeaponsData.playerWeapons.Add(starter);
    }
}