﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private TMP_Text bulletText;
    [SerializeField] private TMP_Text coinsText;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text enemiesText;
    [SerializeField] private TMP_Text levelText;
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private HealthBar reloadBar;
    [SerializeField] private HealthBar dashBar;
    [SerializeField] private PlayerData playerData;
    private float invincibleTime;
    private bool isInvincible = true;

    private bool isReloading;

    private void Start()
    {
        healthBar = GameObject.Find("HealthBar").GetComponent<HealthBar>();
        reloadBar = GameObject.Find("ReloadBar").GetComponent<HealthBar>();
        dashBar = GameObject.Find("DashBar").GetComponent<HealthBar>();
        healthBar.SetMaxHealth(playerData.health);
    }

    public void FixedUpdate()
    {
        UpdateSliders();
        coinsText.text = $"Coins: {playerData.coins}";
        scoreText.text = $"Score: {GameManager.Instance.Score}";
        enemiesText.text = $"Kills: {GameManager.Instance.Kills}";
        levelText.text = $"Level: 1-{LevelManager.Instance.IndexCurrentLevel + 1}";
    }

    private void UpdateSliders()
    {
        healthBar.SetHealth(playerData.health);
        DashHandler();
        AmmoHandler();
    }

    private void DashHandler()
    {
        if (playerData.isInvincible)
            if (isInvincible)
            {
                invincibleTime = Time.time;
                StartCoroutine(UIDashCooldown());
            }

        dashBar.SetHealth(invincibleTime - (Time.time - playerData.dashCdTime));
    }

    private void AmmoHandler()
    {
        var weaponData = playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex];
        //bulletText.text = PlayerData.CreateInstance<PlayerData>().ammo.ToString();
        if (weaponData.GetType().Name.Equals("RangeWeaponData"))
        {
            var rangeWeaponData = (RangeWeaponData) weaponData;
            bulletText.text = $"Player ammo: {playerData.ammo}";
            bulletText.text += $"    {weaponData.name} ammo: {rangeWeaponData.RemainAmmo}";
            if (rangeWeaponData.RemainAmmo <= 0 && playerData.ammo != 0)
            {
                if (!isReloading)
                    StartCoroutine(UICooldown(rangeWeaponData));
                else
                    reloadBar.SetHealth(playerData.time - (Time.time - rangeWeaponData.ReloadTime));

                var timeSpan = TimeSpan.FromSeconds(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
                bulletText.text =
                    $"Player ammo: {playerData.ammo}      {weaponData.name} reload time: {timeSpan:ss\\.ff}";
            }
        }
        else if (weaponData.GetType().Name.Equals("MeleeWeaponData"))
        {
            bulletText.text = $"Player weapon: {weaponData.name}";
        }
    }

    private IEnumerator UICooldown(RangeWeaponData rangeWeaponData)
    {
        isReloading = true;
        reloadBar.SetMaxHealth(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
        yield return new WaitForSeconds(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
        isReloading = false;
    }

    private IEnumerator UIDashCooldown()
    {
        isInvincible = false;
        dashBar.SetMaxHealth(playerData.dashCdTime);
        yield return new WaitForSeconds(playerData.dashCdTime);
        isInvincible = true;
    }
}