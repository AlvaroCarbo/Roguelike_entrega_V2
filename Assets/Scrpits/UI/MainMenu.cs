﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void ChangeScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    public void DeletePlayerPrefs(string key)
    {
        PlayerPrefs.DeleteKey(key);
    }
}