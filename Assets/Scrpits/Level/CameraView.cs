﻿using UnityEngine;

public class CameraView : MonoBehaviour
{
    public Vector3 Position;

    public float orthographicSize;

    public void Start()
    {
        Position = transform.position;
    }
}