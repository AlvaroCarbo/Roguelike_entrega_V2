﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "LevelData/LevelData", order = 1)]
public class LevelData : ScriptableObject
{
    public GameObject initialRoom;
    public GameObject lastRoom;
    public GameObject shopRoom;
    public GameObject[] mainRooms;
    public int minRooms;
    public int maxRooms;
    public GameObject[] enemies;
    [Header("Distance between rooms")] public float distance;
}