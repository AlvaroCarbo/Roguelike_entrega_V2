using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    //TODO: SINGLETON?
    [SerializeField] private List<LevelData> levels;
    public int IndexCurrentLevel { get; private set; }

    public static LevelManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this) Destroy(gameObject);
        Instance = this;

        if (levels == null) return;
        InitializeMap();
    }

    private void Start()
    {
        InitializePlayer();
        InitializeCamera();
    }

    private void InitializeMap()
    {
        //Setting level root
        var levelRoot = Instantiate(new GameObject(), transform.position, Quaternion.identity);
        levelRoot.name = levels[IndexCurrentLevel].name;

        // Generating Starter Room
        var roomPosition = new Vector3();
        GenerateChunk(levels[IndexCurrentLevel].initialRoom, roomPosition, Quaternion.identity);

        // Generating Main Rooms
        var roomCount = Random.Range(levels[IndexCurrentLevel].minRooms, levels[IndexCurrentLevel].maxRooms);
        var shopIndex = Random.Range(1, roomCount > 1 ? roomCount - 1 : roomCount);
        var i = 0;
        for (i++; i < roomCount; i++)
        {
            roomPosition = new Vector3(0, i * levels[IndexCurrentLevel].distance, 0);
            if (shopIndex == i)
            {
                GenerateChunk(
                    levels[IndexCurrentLevel].shopRoom,
                    roomPosition, Quaternion.identity);
                continue;
            }

            GenerateChunk(
                levels[IndexCurrentLevel].mainRooms[Random.Range(0, levels[IndexCurrentLevel].mainRooms.Length)],
                roomPosition, Quaternion.identity);
        }

        // Generating Final room
        roomPosition = new Vector3(0, i * levels[IndexCurrentLevel].distance, 0);
        GenerateChunk(levels[IndexCurrentLevel].lastRoom, roomPosition, Quaternion.identity);
    }

    private void GenerateChunk(GameObject chunk, Vector3 position, Quaternion quaternion)
    {
        var levelChunk = Instantiate(chunk, position, quaternion);
        levelChunk.transform.SetParent(gameObject.transform);
    }

    public void LoadNextLevel()
    {
        IndexCurrentLevel++;
        if (IndexCurrentLevel >= levels.Count) return;
        DestroyMap();
        InitializeMap();
        InitializePlayer();
        InitializeCamera();
    }

    private void DestroyMap()
    {
        var map = gameObject.transform.childCount;
        for (var i = 0; map > i; i++)
        {
            var child = gameObject.transform.GetChild(i).gameObject;
            if (!child.CompareTag("Room")) continue;
            Destroy(child);
        }
    }

    private static void InitializePlayer()
    {
        var player = GameObject.FindWithTag("Player");
        player.transform.position = new Vector3(0, 0, 0);
        player.GetComponent<Player>().playerData.ammo = 100;
    }


    private static void InitializeCamera()
    {
        Camera.main.transform.position = new Vector3(0, 0.25f, -10);
    }
}