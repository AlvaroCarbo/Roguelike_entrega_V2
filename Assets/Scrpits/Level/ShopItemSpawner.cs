﻿using System.Collections.Generic;
using UnityEngine;

public class ShopItemSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> shopItems;

    [SerializeField] private Transform[] spawnPoints;

    private void Start()
    {
        if (shopItems == null) return;
        if (spawnPoints == null) return;
        foreach (var point in spawnPoints)
        {
            var itemIndex = Random.Range(0, shopItems.Count);
            var item = Instantiate(shopItems[itemIndex], point.position, Quaternion.identity);
            item.transform.SetParent(gameObject.transform);
            shopItems.RemoveAt(itemIndex);
            var itemRotation = item.transform.rotation;
            itemRotation.z = -0.415f;
            item.transform.rotation = itemRotation;
        }
    }
}