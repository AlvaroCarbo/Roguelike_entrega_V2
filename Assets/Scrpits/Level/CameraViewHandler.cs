﻿using UnityEngine;

public class CameraViewHandler : MonoBehaviour
{
    [SerializeField] private GameObject cameraViewGameObject;
    private GameObject levelParent;

    private void Awake()
    {
        // Referencing level root GameObject
        if (levelParent != null) return;
        levelParent = gameObject.transform.parent.parent.parent.parent.gameObject;
    }

    private void Start()
    {
        // Setting camera view from the previous room
        if (cameraViewGameObject != null) return;
        var levelManager = levelParent.transform.parent;
        for (var i = 0; i < levelManager.childCount; i++)
        {
            if (!Equals(levelParent, levelManager.GetChild(i).gameObject)) continue;
            cameraViewGameObject = levelManager.GetChild(i - 1).gameObject.transform
                .GetChild(levelManager.GetChild(i - 1).gameObject.transform.childCount - 1).gameObject;
        }
    }

    public void Enter()
    {
        if (cameraViewGameObject == null) return;
        CameraManager.Instance.ChangeToRoom(cameraViewGameObject.GetComponent<CameraView>());
    }
}