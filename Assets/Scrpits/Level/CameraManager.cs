﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public delegate void MoveCamera();

    private Camera _camera;
    public static CameraManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this) Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        _camera = Camera.main;
    }

    public void ChangeToRoom(CameraView cameraView)
    {
        _camera.transform.position = cameraView.Position;
        _camera.orthographicSize = cameraView.orthographicSize;
    }
}