﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveSpawner : MonoBehaviour
{
    public delegate void ClearRoomHandler();

    [SerializeField] private GameObject[] enemies;
    [SerializeField] private Transform[] spawnPoints;
    [SerializeField] private int minWaves;
    [SerializeField] private int maxWaves;

    private WaveState currentState;
    private int currentWave;
    private int waves;

    private void Start()
    {
        currentState = WaveState.Waiting;
        waves = Random.Range(minWaves, maxWaves);
    }

    private void FixedUpdate()
    {
        switch (currentState)
        {
            case WaveState.Waiting:
                break;

            case WaveState.Spawning:
                if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0) Spawn();
                if (currentWave >= waves) currentState = WaveState.Finished;
                break;

            case WaveState.Finished:
                var objects = GameObject.FindGameObjectsWithTag("Enemy");
                if (objects.Length <= 0)
                    DestroySpawner();
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (!other.gameObject.Equals(GameObject.FindWithTag("Player"))) return;
        if (currentState != WaveState.Waiting) return;
        currentState = WaveState.Spawning;
    }

    public event ClearRoomHandler Cleared;

    private void Spawn()
    {
        var enemiesToSpawn = Random.Range(2, 5);
        for (var i = 0; i < enemiesToSpawn; i++)
            Instantiate(
                enemies[Random.Range(0, enemies.Length)],
                spawnPoints[Random.Range(0, spawnPoints.Length)].position,
                Quaternion.identity);

        currentWave++;
    }

    private void DestroySpawner()
    {
        GameManager.Instance.Score += 20;
        Cleared?.Invoke();
        Destroy(gameObject);
    }

    private enum WaveState
    {
        Waiting,
        Spawning,
        Finished
    }
}