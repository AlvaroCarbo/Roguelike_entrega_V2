﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerWeaponsData", menuName = "ScriptableObjects/PlayerWeaponsData")]
public class PlayerWeaponsData : ScriptableObject
{
    public List<WeaponData> playerWeapons;
    public int weaponIndex;
}