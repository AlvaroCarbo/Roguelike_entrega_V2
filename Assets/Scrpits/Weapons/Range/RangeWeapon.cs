﻿using System.Collections;
using UnityEngine;

public class RangeWeapon : Weapon
{
    [Header("Game Object")] [SerializeField] [Tooltip("Game Object used by the ranged weapon.")]
    private GameObject bullet;

    protected override void Attack()
    {
        base.Attack();
        if (!playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex].GetType().Name
            .Equals("RangeWeaponData")) return;
        var playerWeapon =
            (RangeWeaponData) playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex];
        if (playerData.ammo <= 0 && playerWeapon.RemainAmmo <= 0) return;
        if (playerWeapon.canShoot)
            PullTrigger(playerWeapon);
    }

    private void PullTrigger(RangeWeaponData weapon)
    {
        var bulletsToShoot = weapon.BulletsXShoot <= weapon.RemainAmmo
            ? weapon.BulletsXShoot
            : weapon.BulletsXShoot - weapon.RemainAmmo;
        for (var i = 0; i < bulletsToShoot; i++) //TODO: refactor this iterator
        {
            var angle = weapon.angleFromOtherBullet;
            //int temp;
            //temp = (i % 2 != 0) ? i : -i;
            angle = i % 2 != 0 ? angle : -angle;
            //Debug.Log(temp + " " + angle);#1#
            //Debug.Log(bulletsToShoot % 2 == 0);
            if (bulletsToShoot % 2 != 0)
            {
                angle = i == 0 ? 0 : angle;
                angle = i > 2 ? angle * 2 : angle;
                angle = i > 4 ? angle * 3 : angle;
                Shoot(weapon, angle);
            }
            else
            {
                switch (i)
                {
                    case 0:
                        Shoot(weapon, 2.5f);
                        break;
                    case 1:
                        Shoot(weapon, -2.5f);
                        break;
                    case 2:
                        Shoot(weapon, 7.5f);
                        break;
                    case 3:
                        Shoot(weapon, -7.5f);
                        break;
                }
            }

            weapon.RemainAmmo -= 1;
        }

        StartCoroutine(weapon.RemainAmmo > 0 ? ShootCooldown(weapon) : Reloading(weapon));

        rigidbody2D.AddForce(-playerData.mouseDirection * weapon.SetbackMomentum, ForceMode2D.Impulse);
    }

    private void Shoot(RangeWeaponData weapon, float angle)
    {
        //TODO: refactor this method
        var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
        var bulletData = weapon.bulletData;
        shoot.GetComponent<BulletBehaviour>().SetData(bulletData, playerData, angle);
        //Debug.DrawRay(transform.GetChild(0).position, direction, Color.red);
    }

    private IEnumerator Reloading(RangeWeaponData weapon)
    {
        weapon.canShoot = false;
        playerData.time = Time.time;
        yield return new WaitForSeconds(weapon.ReloadTime);
        weapon.RemainAmmo = playerData.ammo > weapon.MaxAmmo ? weapon.MaxAmmo : playerData.ammo;
        playerData.ammo -= playerData.ammo > 0 ? weapon.RemainAmmo : 0;
        weapon.canShoot = true;
    }

    private static IEnumerator ShootCooldown(RangeWeaponData weapon)
    {
        weapon.canShoot = false;
        yield return new WaitForSeconds(weapon.FireRate);
        weapon.canShoot = true;
    }
}