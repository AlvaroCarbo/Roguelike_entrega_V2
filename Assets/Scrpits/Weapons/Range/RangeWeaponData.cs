﻿using UnityEngine;

[CreateAssetMenu(fileName = "RangeWeaponData", menuName = "Weapons/RangeWeaponData", order = 1)]
public class RangeWeaponData : WeaponData
{
    //private int Damage;
    public int RemainAmmo;
    public int MaxAmmo;
    public float FireRate;
    public float ReloadTime;
    public int BulletsXShoot;
    public float angleFromOtherBullet;
    public float SetbackMomentum;
    public bool canShoot;


    public BulletData bulletData;
    //public List<BulletData> bullets;
    //TODO index list

    private void OnEnable()
    {
        canShoot = true;
        RemainAmmo = MaxAmmo;
    }
}