﻿using UnityEngine;

[CreateAssetMenu(fileName = "BulletData", menuName = "Weapons/BulletData", order = 1)]
public class BulletData : ScriptableObject
{
    public float Force;
    public Sprite bulletSprite;
}