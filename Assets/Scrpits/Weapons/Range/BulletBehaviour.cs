﻿using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    private float _angle;
    private BulletData _bulletData;
    private PlayerData _playerData;

    private Rigidbody2D _rigidbody2D;
    private bool _set;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        if (!_set) return;
        Vector2 direction = Quaternion.AngleAxis(_angle, Vector3.forward) * _playerData.mouseDirection;
        _rigidbody2D.AddForce(direction * _bulletData.Force, ForceMode2D.Impulse);
        _spriteRenderer.sprite = _bulletData.bulletSprite;
        transform.up = direction;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall") || collision.CompareTag("Destructable")) Destroy(gameObject);

        if (gameObject.CompareTag("EnemyBullet") && collision.CompareTag("Player"))
        {
            //Destroy(gameObject);
        }
    }

    public void SetData(BulletData bulletData, PlayerData playerData, float angle)
    {
        _bulletData = bulletData;
        _playerData = playerData;
        _angle = angle;
        _set = true;
    }
}