﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class MeleeWeapon : Weapon
{
    private static readonly int AttackAnimation = Animator.StringToHash("Attack");
    private Animator animator;
    private new CircleCollider2D collider2D;

    public override void Awake()
    {
        base.Awake();
        collider2D = GetComponent<CircleCollider2D>();
        animator = GetComponentInChildren<Animator>();
        collider2D.enabled = false;
    }

    protected override void Attack()
    {
        base.Attack();
        if (playerData.canAttack)
            StartCoroutine(MeleeAttack());
    }

    private IEnumerator MeleeAttack()
    {
        playerData.canAttack = false;
        var color = Color.red;
        animator.SetBool(AttackAnimation, true);
        collider2D.enabled = true;
        spriteRenderer.color = color;
        yield return new WaitForSeconds(0.5f);
        playerData.canAttack = true;
        animator.SetBool(AttackAnimation, false);
        collider2D.enabled = false;
        color = Color.white;
        spriteRenderer.color = color;
    }
}