﻿using UnityEngine;

[CreateAssetMenu(fileName = "MeleeWeaponData", menuName = "Weapons/MeleeWeaponData", order = 1)]
public class MeleeWeaponData : WeaponData
{
    public int damage;
}