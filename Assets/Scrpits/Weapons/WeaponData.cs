﻿using UnityEngine;

public abstract class WeaponData : ScriptableObject
{
    public Sprite weaponSprite;
    public GameObject weaponPrefab;
}