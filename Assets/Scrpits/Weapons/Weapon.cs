﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
public class Weapon : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public new Rigidbody2D rigidbody2D;
    [SerializeField] private InputData inputData;
    [SerializeField] public PlayerData playerData;

    [SerializeField] [Tooltip("Data handled by the weapon hold by the player.")]
    private WeaponData weapon;

    public virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        switch (playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex].GetType().Name)
        {
            case "RangeWeaponData":
            {
                var playerWeapon =
                    (RangeWeaponData) playerData.playerWeaponsData.playerWeapons[
                        playerData.playerWeaponsData.weaponIndex];
                playerWeapon.canShoot = true;
                playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex] = playerWeapon;
                break;
            }
            case "MeleeWeaponData":
                playerData.canAttack = true;
                break;
        }

        SetWeapon();
    }

    public virtual void Update()
    {
        ChangeWeapon();
        if (inputData.mouseClickInput) Attack();
        RotateWeapon();
    }

    protected virtual void Attack()
    {
    }

    private void RotateWeapon()
    {
        spriteRenderer.flipX = false;
        var transformGO = transform;
        var position = transformGO.position;
        var parentPos = transformGO.parent.position;
        if (inputData.mouseScreenPoint.x > parentPos.x)
        {
            position = new Vector3(parentPos.x + 0.5f, position.y, position.z);
            spriteRenderer.flipY = false;
        }
        else
        {
            position = new Vector3(parentPos.x - 0.5f, position.y, position.z);
            spriteRenderer.flipY = true;
        }

        transform.position = position;

        transform.rotation = Quaternion.Euler(0f, 0f,
            Mathf.Atan2(playerData.mouseDirection.y,
                playerData.mouseDirection.x) * Mathf.Rad2Deg);
    }

    private void ChangeWeapon()
    {
        var weaponIndex = playerData.playerWeaponsData.weaponIndex;
        var maxIndex = playerData.playerWeaponsData.playerWeapons.Count;
        weaponIndex += inputData.mouseScrollInput;
        if (weaponIndex == playerData.playerWeaponsData.weaponIndex) return;
        weaponIndex = weaponIndex < 0 ? maxIndex + weaponIndex : weaponIndex >= maxIndex ? 0 : weaponIndex;
        playerData.playerWeaponsData.weaponIndex = weaponIndex;
        SetWeapon();
    }

    private void SetWeapon()
    {
        if (weapon == playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex]) return;
        weapon = playerData.playerWeaponsData.playerWeapons[playerData.playerWeaponsData.weaponIndex];
        var newWeapon = Instantiate(weapon.weaponPrefab, transform.position, Quaternion.identity);
        newWeapon.transform.SetParent(GameObject.FindWithTag("Player").transform);
        newWeapon.transform.SetAsFirstSibling();
        Destroy(gameObject);
    }
}