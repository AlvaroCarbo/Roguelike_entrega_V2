﻿using UnityEngine;

public abstract class CharacterData : ScriptableObject
{
    public float velocity;
    public int health;
    public bool isInvincible;
    public bool canAttack;
    public bool isHitted;
}