﻿using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "CharacterData/EnemyData", order = 1)]
public class EnemyData : CharacterData
{
    public int damage;
}