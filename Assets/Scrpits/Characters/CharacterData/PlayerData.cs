﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "CharacterData/PlayerData", order = 1)]
public class PlayerData : CharacterData
{
    public Vector2 moveDirection;
    public Vector2 mouseDirection;
    public bool canDash;
    public float dashCdTime;
    public float dashForce;
    public PlayerWeaponsData playerWeaponsData;
    public int ammo;
    public int lifes;
    public float time;
    public int coins;
    public int highScore;

    private void OnEnable()
    {
        canDash = true;
        ammo = 50;
        time = Time.time;
        isHitted = false;
        canAttack = true;
        isInvincible = false;
        health = 10;
    }
}