﻿using System.Collections;
using UnityEngine;

public class Enemy : Character
{
    private static readonly int Explosion = Animator.StringToHash("Explosion");
    [SerializeField] public EnemyData enemyData;
    [SerializeField] private bool canExplode;
    [SerializeField] private bool canMove;
    [SerializeField] private bool canShoot;
    [SerializeField] private GameObject enemyBullet;
    private new Collider2D collider2D;
    private bool death;

    private GameObject player;

    private void Awake()
    {
        transform.SetParent(GameObject.Find("LevelManager").transform);
    }

    public override void Start()
    {
        base.Start();
        collider2D = GetComponent<Collider2D>();
        player = GameObject.Find("Player");
    }


    private void FixedUpdate()
    {
        if (canMove)
            Move();
        if (canShoot)
            Shoot();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Player")) return;
        if (collision.gameObject.GetComponent<Rigidbody2D>() == null) return;
        var direction = collision.gameObject.GetComponent<Rigidbody2D>().position -
                        GetComponent<Rigidbody2D>().position;
        collision.gameObject.GetComponent<Rigidbody2D>().AddForce(direction * 10, ForceMode2D.Impulse);
        if (!canExplode) return;
        canMove = false;
        GetComponent<Animator>().SetBool(Explosion, true);
        StartCoroutine(DestroyItself());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (death) return;
        if (!collision.CompareTag("Bullet") && !collision.CompareTag("MeleeWeapon")) return;
        death = true;
        GameManager.Instance.Score += 5;
        GameManager.Instance.Kills++;
        KillEnemy();
        if (!collision.CompareTag("Bullet")) return;
        Destroy(collision.gameObject);
    }

    private void Shoot()
    {
        var position = transform.position;
        Vector2 direction = player.transform.position - position;
        var shoot = Instantiate(enemyBullet, position, Quaternion.identity);
        shoot.GetComponent<Rigidbody2D>().AddForce(direction.normalized * 10f, ForceMode2D.Impulse);
        shoot.transform.up = direction;
        StartCoroutine(ShootCooldown());
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(2f);
        canShoot = true;
    }

    public override void Move()
    {
        // Move our position a step closer to the target.
        var step = enemyData.velocity * Time.deltaTime; // calculate distance to move
        if (!player) return;
        Transform transformGO;
        var position = player.transform.position;
        (transformGO = transform).position = Vector3.MoveTowards(gameObject.transform.position, position, step);
        spriteRenderer.flipX = transformGO.position.x > position.x;
    }

    private IEnumerator DestroyItself()
    {
        collider2D.enabled = false;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    private void KillEnemy()
    {
        GetComponent<AudioSource>().Play();
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Animator>().enabled = false;
        GetComponent<PolygonCollider2D>().enabled = false;
        Destroy(gameObject, 0.2f);
    }
}