﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(InputController))]
public class Player : Character
{
    private static readonly int IsMoving = Animator.StringToHash("isMoving");
    private static readonly int IsHitted = Animator.StringToHash("isHitted");

    [Header("Data")] [SerializeField] [Tooltip("Input data from the player.")]
    private InputData inputData;

    [SerializeField] [Tooltip("Data handled by the player.")]
    public PlayerData playerData;

    private Animator animator;
    private new Rigidbody2D rigidbody2D;

    public override void Start()
    {
        base.Start();
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        playerData.isHitted = false;
        playerData.canDash = true;
    }

    protected override void Update()
    {
        playerData.moveDirection = Vector2.ClampMagnitude(inputData.axisInputDirection, 1);
    }

    private void FixedUpdate()
    {
        RotateCharacter();
        if (inputData.spaceInput) Dash();
        Move();
        playerData.mouseDirection = (inputData.mouseScreenPoint - GetComponent<Rigidbody2D>().position).normalized;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Enemy")) return;
        if (!playerData.isInvincible)
        {
            if (playerData.isHitted) return;
            playerData.health -= other.gameObject.GetComponent<Enemy>().enemyData.damage;
            TakeDamage(other.gameObject);
        }
        else
        {
            // Kill Enemies when invincible
            Destroy(other.gameObject.gameObject);
            GameManager.Instance.Score += 2;
            GameManager.Instance.Kills++;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
        {
            Destroy(collision.gameObject);
            playerData.coins++;
            GameManager.Instance.Score++;
        }

        if (collision.CompareTag("Drop"))
        {
            if (playerData.health >=
                GameManager.Instance.PlayerMaxHealth) return;
            collision.GetComponent<IItem>().Interact(playerData);
            GameManager.Instance.Score++;
        }

        if (collision.CompareTag("EnemyBullet"))
        {
            if (!playerData.isInvincible)
            {
                if (!playerData.isHitted)
                {
                    playerData.health--;
                    TakeDamage(collision.gameObject);
                    Destroy(collision.gameObject);
                }
            }
            else
            {
                Destroy(collision.gameObject);
            }
        }

        if (collision.CompareTag("ShopItem"))
            collision.gameObject.GetComponent<IItem>().Interact(playerData);


        if (!playerData.isInvincible || !collision.CompareTag("Destructable")) return;
        GameObject o;
        (o = collision.gameObject).GetComponent<DestructibleObjects>().DropItem();
        Destroy(o);
        GameManager.Instance.Score++;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Door")) return;
        var door = collision.GetComponent<CameraViewHandler>();
        door.Enter();
    }

    private void RotateCharacter()
    {
        spriteRenderer.flipX = !(inputData.mouseScreenPoint.x > transform.position.x);
    }


    public override void Move()
    {
        if (inputData.axisInputDirection.x != 0 || inputData.axisInputDirection.y != 0)
        {
            animator.SetBool(IsMoving, true);
            rigidbody2D.AddForce(playerData.moveDirection * playerData.velocity, ForceMode2D.Force);
        }
        else
        {
            animator.SetBool(IsMoving, false);
        }
    }

    private void Dash()
    {
        if (!playerData.canDash) return;
        StartCoroutine(InvincibleTime());
        rigidbody2D.AddForce(playerData.moveDirection * playerData.dashForce, ForceMode2D.Impulse);
        StartCoroutine(DashCooldown());
    }

    private IEnumerator InvincibleTime()
    {
        playerData.isInvincible = true;
        transform.GetChild(2).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        transform.GetChild(2).gameObject.SetActive(false);
        playerData.isInvincible = false;
    }

    private IEnumerator DashCooldown()
    {
        playerData.canDash = false;
        yield return new WaitForSeconds(playerData.dashCdTime);
        playerData.canDash = true;
    }

    private void TakeDamage(GameObject collision)
    {
        if (playerData.health <=
            0)
        {
            GameManager.Instance.LoseGame();
            Destroy(gameObject);
        }

        StartCoroutine(HurtAnimation());
        if (collision.GetComponent<Rigidbody2D>() == null) return;
        var direction = collision.GetComponent<Rigidbody2D>().position -
                        rigidbody2D.position;
        collision.GetComponent<Rigidbody2D>().AddForce(direction * 20, ForceMode2D.Impulse);
    }

    private IEnumerator HurtAnimation()
    {
        spriteRenderer.color = Color.red;
        playerData.isHitted = true;
        animator.SetBool(IsHitted, true);
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
        animator.SetBool(IsHitted, false);
        yield return new WaitForSeconds(0.4f);
        playerData.isHitted = false;
    }
}