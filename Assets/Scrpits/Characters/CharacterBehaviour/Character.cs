﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
public abstract class Character : MonoBehaviour
{
    [HideInInspector] public SpriteRenderer spriteRenderer;

    public virtual void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected virtual void Update()
    {
    }

    public virtual void Move()
    {
    }

    public virtual void Attack()
    {
    }
}