﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject Player;
    [SerializeField] private PlayerData playerData;
    [SerializeField] private GameObject endPanel;
    [SerializeField] private Button restartButton;
    [SerializeField] private InputData inputData;
    public bool pauseLevel;
    [SerializeField] private AudioSource musicSource;
    private GameObject destructObjects;
    private GameObject enemies;
    private GameObject grid;
    private int highScore;
    private GameObject hud;
    private GameObject levelManager;

    public GameManager(List<int> spawners)
    {
        Spawners = spawners;
    }

    public int PlayerMaxHealth { get; set; } = 10;
    public int Kills { get; set; }
    public int Score { get; set; }
    private List<int> Spawners { get; }

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this) Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager");
        destructObjects = GameObject.Find("DestructibleObjects");
        grid = GameObject.FindWithTag("Room");
        enemies = GameObject.FindWithTag("Enemy");
        Player = GameObject.FindWithTag("Player");
        hud = GameObject.Find("HUD");
        ActivateSceneGO(true);
        playerData = Player.GetComponent<Player>().playerData;
        restartButton.onClick.AddListener(RestartGame);
        Score = 0;
        LoadGame();
        PlayerMaxHealth = 10;
        playerData.health = PlayerMaxHealth;
    }

    private void Update()
    {
        if (inputData.escapeInput) pauseLevel = !pauseLevel;
        if (pauseLevel)
            for (var i = 0; i < levelManager.transform.childCount; i++)
                levelManager.transform.GetChild(i).gameObject.SetActive(false);
        else
            for (var i = 0; i < levelManager.transform.childCount; i++)
                levelManager.transform.GetChild(i).gameObject.SetActive(true);
    }

    public void SaveGame()
    {
        PlayerPrefs.SetInt("coins", playerData.coins);
        PlayerPrefs.SetInt("Score", Score);
        if (Score >= highScore)
        {
            highScore = Score;
            PlayerPrefs.SetInt("HighScore", highScore);
        }

        PlayerPrefs.Save();
    }

    private void LoadGame()
    {
        playerData.coins = PlayerPrefs.GetInt("coins");
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        playerData.health = PlayerMaxHealth;
    }

    public void LoseGame()
    {
        AudioEffectsManager.Instance.PlaySound(0);
        musicSource.Stop();
        SaveGame();
        StartCoroutine(LoadMainScene());
        //SET LOSE UI
    }

    private static IEnumerator LoadMainScene()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(0);
    }

    public void MuteMusic(float seconds)
    {
        AudioManager.Instance.audioData.mute = true;
        StartCoroutine(EnableMusic(seconds));
    }

    private IEnumerator EnableMusic(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        AudioManager.Instance.audioData.mute = false;
    }

    private void ActivateSceneGO(bool flag)
    {
        hud.SetActive(flag);
        grid.SetActive(flag);
        enemies?.SetActive(flag);
        destructObjects?.SetActive(flag);
    }
}