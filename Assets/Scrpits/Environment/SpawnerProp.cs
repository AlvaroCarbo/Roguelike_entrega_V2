﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnerProp : DestructibleObjects
{
    [Tooltip("List of different enemies to Spawn.")] [SerializeField]
    private List<GameObject> enemies;

    [Tooltip("Time between Spawn calls.")] [SerializeField]
    private float time;

    [Tooltip("Repetitions rate in a single call.")] [SerializeField]
    private float repeatRate;

    private int enemiesCount;

    private void Awake()
    {
        enemiesCount = enemies.Count;
    }

    private void Start()
    {
        InvokeRepeating(nameof(Spawn), time, repeatRate);
    }

    private void OnDestroy()
    {
        Destroy(gameObject);
        //GameManager.Instance.Spawners[GameManager.Instance.CurrentRoom]--;
    }

    private void Spawn()
    {
        if (GameManager.Instance.pauseLevel) return;
        var randomEnemyIndex = Random.Range(0, enemiesCount);
        var enemyToSpawn = enemies[randomEnemyIndex];
        var posToSpawn = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1));
        Instantiate(enemyToSpawn, transform.position + posToSpawn, Quaternion.identity);
    }
}