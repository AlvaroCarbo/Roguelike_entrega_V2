﻿using UnityEngine;

public class DoorHandler : MonoBehaviour
{
    [SerializeField] private WaveSpawner waveSpawner;
    [SerializeField] private GameObject openedDoor;
    [SerializeField] private GameObject closedDoor;

    public void Start()
    {
        waveSpawner.Cleared += OpenDoor;
    }

    public void OnDestroy()
    {
        waveSpawner.Cleared -= OpenDoor;
    }

    private void OpenDoor()
    {
        closedDoor.SetActive(false);
        openedDoor.SetActive(true);
        Destroy(gameObject); // It is possible if its attached to the closed door.
    }
}