﻿using System.Linq;
using UnityEngine;

public class ShopWeaponItem : ShopItem, IItem
{
    [SerializeField] private WeaponData weaponData;

    public new void Awake()
    {
        base.Awake();
    }

    public new void Start()
    {
        base.Start();
    }

    public void Interact(PlayerData playerData)
    {
        if (playerData.coins < price) return;
        if (playerData.playerWeaponsData.playerWeapons.Any(weapon => weapon == weaponData)) return;
        playerData.playerWeaponsData.playerWeapons.Add(weaponData);
        playerData.coins -= price;
        Destroy(gameObject);
    }
}