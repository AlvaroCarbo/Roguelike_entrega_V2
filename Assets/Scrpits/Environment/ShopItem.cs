﻿using TMPro;
using UnityEngine;

public class ShopItem : MonoBehaviour
{
    public string itemName;
    public int price;
    public TMP_Text nameText;
    private TMP_Text priceText;

    public void Awake()
    {
        nameText = transform.GetChild(0).GetComponent<TMP_Text>();
        priceText = transform.GetChild(1).GetComponent<TMP_Text>();
    }

    public void Start()
    {
        nameText.text = itemName;
        priceText.text = $"Price: {price}";
    }
}