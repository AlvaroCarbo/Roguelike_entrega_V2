﻿using UnityEngine;

public class StairsHandler : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        AudioEffectsManager.Instance.PlaySound(1);
        GameManager.Instance.MuteMusic(3f);
        GameManager.Instance.Score += 100;
        GameManager.Instance.SaveGame();
        LevelManager.Instance.LoadNextLevel();
    }
}