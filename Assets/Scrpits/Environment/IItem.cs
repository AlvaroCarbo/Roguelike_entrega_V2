﻿public interface IItem
{
    void Interact(PlayerData playerData);
}