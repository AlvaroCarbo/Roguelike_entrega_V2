﻿using UnityEngine;

public class DestructibleObjects : MonoBehaviour
{
    [Tooltip("List of items to spawn when destroyed.")] [SerializeField]
    private GameObject[] items;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Bullet") && !collision.CompareTag("EnemyBullet") &&
            !collision.CompareTag("MeleeWeapon")) return;
        Destroy(gameObject);
        DropItem();
        if (collision.CompareTag("Bullet") || collision.CompareTag("MeleeWeapon"))
            GameManager.Instance.Score++;
    }

    public void DropItem()
    {
        var item = Instantiate(items[Random.Range(0, items.Length)], transform.position, Quaternion.identity);
        item.transform.SetParent(gameObject.transform.parent);
    }
}