﻿using UnityEngine;

public class ShopAmmoItem : ShopItem, IItem
{
    [SerializeField] private int ammoAmount;

    public new void Awake()
    {
        base.Awake();
    }

    public new void Start()
    {
        base.Start();
        nameText.text = $"x{ammoAmount} {itemName}";
    }

    public void Interact(PlayerData playerData)
    {
        if (playerData.coins < price) return;
        playerData.ammo += ammoAmount;
        playerData.coins -= price;
        Destroy(gameObject);
    }
}