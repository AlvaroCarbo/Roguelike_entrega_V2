﻿using UnityEngine;

public class HeartItem : MonoBehaviour, IItem
{
    private const int MinimumHealth = 1;
    private const int MAXHealth = 3;

    public void Interact(PlayerData data)
    {
        data.health += HealthToRestore();
        Destroy(gameObject);
    }

    private static int HealthToRestore()
    {
        return Random.Range(MinimumHealth, MAXHealth);
    }
}